
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";



/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  ehrId = "";
  $.ajaxSetup({
    headers: {
        "Ehr-Session": sejniID
    }
});
$.ajax({
    url: baseUrl + "/ehr",
    type: 'POST',
    success: function (data) {
        var ehrId = data.ehrId;
        $("#header").html("EHR: " + ehrId);

        // build party data
        var partyData = {
            firstNames: "Lionel",
            lastNames: "Messi",
            dateOfBirth: "24.6.1987",
            partyAdditionalInfo: [
                {
                    key: "ehrId",
                    value: ehrId
                }
            ]
        };
        $.ajax({
            url: baseUrl + "/demographics/party",
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(partyData),
            success: function (party) {
                if (party.action == 'CREATE') {
                    $("#result").html("Created: " + party.meta.href);
                }
            }
        });
    }
});

  // TODO: Potrebno implementirati

  return ehrId;
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija



    function myFunction() { 
        
        document.getElementById("alert").innerHTML = "";
        var data = document.getElementById("data");  
        var ime = data.elements["fname"].value;
        var priimek = data.elements["lname"].value;
         
       var e = document.getElementById("sp");
       var spol = e.options[e.selectedIndex].value;
       
       var visina = data.elements["visina"].value;
       var teza = data.elements["teza"].value;
       var indeks = pomoc(teza, visina);
       var podatek = "";
       var sex = "";
       var alk = document.getElementById("alk").options[document.getElementById("alk").selectedIndex].value;
       var cig = document.getElementById("cig").options[document.getElementById("cig").selectedIndex].value;
        var letnica = data.elements["year"].value;
        var day = data.elements["day"].value;
        var month =  data.elements["month"].value;
        var predvideno = 83 - (2017 - letnica);
        
        if(alk == 2){
            predvideno -= 6;
        }if(cig == 2){
            predvideno -= 10;
        }
      if(spol == 2){
          sex = "Pozdravljen gospod"
          predvideno -= 5;
      }else if(spol == 3){
           sex = "Pozdravljena gospa"
      }
     
     if(spol==2 && indeks<20 || spol==3 && indeks<18.6){
           podatek = "Podhranjenost"
           predvideno -= 5;
           
       }else if(spol==2 && indeks<25 || spol==3 && indeks<25){
           podatek = "Normalna teža"
           
       }else if(spol==2 && indeks<30 || spol==3 && indeks<27){
           podatek = "Prekomerna teža"
           predvideno -= 7;
           
       }else if(spol==2 && indeks<40 || spol==3 && indeks<30){
           podatek = "Debelost 2. stopnje"
           predvideno -= 15;
       }else{
           podatek = "Debelost 3. stopnje"
           predvideno -= 19;
       }
        var pro = procent((2017 - letnica), predvideno);
        pro = pro.toFixed(0);
  
        var tekst = "Živeli boste še "+predvideno+" let, izkoristite jih pametno!";
         if(predvideno < 0){
           tekst = "Nažalost ste že pokojni ...";
           pro = 100;
       }
       if(predvideno>120){
           tekst = "Niste še rojeni!";
           pro = 0;
       }
         
      if(day != "" && month != "" && letnica != "" && alk != 1 && cig != 1 && spol != 1 && ime != "" && priimek != "" && visina != "" && teza != ""){
    
          document.getElementById("demo").innerHTML = sex+" "+ime+" "+priimek+"!<br><h4> Vaš indeks telesne mase(ITM):</h4> "+indeks+" ("+ podatek+ ")";
          document.getElementById("demo3").innerHTML = "<strong>"+tekst+"</strong>";
            document.getElementById("proc").innerHTML = "Trenutno ste na "+pro+"% svojega življenjaaa.";
             scrollDown();
           document.getElementById("poskus").innerHTML = izrisi(pro);
         
      }

      else{
           document.getElementById("demo").innerHTML = "";
             document.getElementById("proc").innerHTML = "";
          document.getElementById("demo3").innerHTML = "";
           document.getElementById("alert").innerHTML = "Izpolniti je potrebno vsa polja!";
            document.getElementById("poskus").innerHTML = izrisi(0);
      }
      
     }
     
     function brisi() {
         scrollUp();
    document.getElementById("data").reset();
     document.getElementById("demo").innerHTML = "";
    document.getElementById("proc").innerHTML = "";
    document.getElementById("demo3").innerHTML = "";
    document.getElementById("alert").innerHTML = "";
     document.getElementById("poskus").innerHTML = izrisi(0);
 
    
     
}
function pomoc(teza, visina){
    var pomoc = visina / 100;
     var ind = teza / (pomoc * pomoc);
    return ind.toFixed(1);
}

function scrollDown() {
    window.scrollTo(0, 1500);
}
function scrollUp() {
    window.scrollTo(0, 0);
}
function izrisi(a){
         
         var vrednost = a;
      var svg=d3.select("svg");
      var g=svg.append("g").attr("transform","translate(150,150)");
      var domain = [0,100];
      
      var gg = viz.gg()
    	.domain(domain)
    	.outerRadius(150)
    	.innerRadius(30)
    	.value(vrednost)
      
      gg.defs(svg);
      g.call(gg);  
}
function procent(letnica, imam){
    return (letnica/(letnica + imam)) * 100;
}
var ehr = [];
var ehrime = [];
var ehrVisina = [];
var ehrTeza = [];

function najdi(ehr){
    for(var i=0; i<ehr.length; i++){
        if(ehr[i] == null){
            return i;
        }
    }
}


function kreirajEHRzaBolnika() {
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var datumRojstva = $("#kreirajDatumRojstva").val();
	var telesnaTeza = $("#dodajVitalnoTelesnaVisina").val();
	var telesnaVisina = $("#dodajVitalnoTelesnaTeza").val();

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}],
	
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                  
		                if (party.action == 'CREATE') {
		                    var a = najdi(ehr);
		                    ehr[a] = ehrId;
		                    ehrime[a] = ime+""+priimek;
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}


  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
  });
 	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});
	
function preberiEHRodBolnika() {
	sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
	    	   
				var party = data.party;
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Oseba '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth[0]+""+party.dateOfBirth[1]+""+party.dateOfBirth[2]+""+party.dateOfBirth[3]+""+party.dateOfBirth[4]+""+party.dateOfBirth[5]+""+party.dateOfBirth[6]+""+party.dateOfBirth[7]+""+party.dateOfBirth[8]+""+party.dateOfBirth[9] +
          "'.</span>");
          obPritisku(party.firstNames, party.lastNames, party.dateOfBirth);
          if(party.fistNames != "Lionel"){
                            	sessionId = getSessionId();

                        	var ehrId = $("#meritveVitalnihZnakovEHRid").val();
                        	var kaj = document.getElementById("data"); 
                
              
              				$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/" + "weight",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
						    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna teža</th></tr>";
						        for (var i in res) {
						            kaj.elements["teza"].value = res[i];
						            results += "<tr><td>" + res[i].time +
                          "</td><td class='text-right'>" + res[i].weight + " " 	+
                          res[i].unit + "</td>";
						        }
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").append(results);
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
          }
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}
function obPritisku(ime, priimek, date){
      document.getElementById("alert").innerHTML = "";
   var kaj = document.getElementById("data");  
   kaj.elements["fname"].value = ime;
   kaj.elements["lname"].value = priimek;
    kaj.elements["year"].value = date[0]+""+date[1]+""+date[2]+""+date[3];
    kaj.elements["month"].value = date[5]+""+date[6];
    kaj.elements["day"].value = date[8]+""+date[9];
            kaj.elements["visina"].value = "";
      kaj.elements["teza"].value = "";
     document.getElementById('sp').value = 1;
    document.getElementById('alk').value = 1;
    document.getElementById('cig').value = 1;
    
    if(ime == "Lionel" && priimek == "Messi"){
         kaj.elements["visina"].value = "169";
      kaj.elements["teza"].value = "69";
     document.getElementById('sp').value = 2;
    document.getElementById('alk').value = 3;
    document.getElementById('cig').value = 3;
    
    } else if(ime == "LeBron" && priimek == "James"){
        
           kaj.elements["visina"].value = "203";
          kaj.elements["teza"].value = "108";
         document.getElementById('sp').value = 2;
        document.getElementById('alk').value = 2;
         document.getElementById('cig').value = 3;
         
    } else if(ime == "Ivan" && priimek == "Perišić"){
        
        kaj.elements["visina"].value = "186";
          kaj.elements["teza"].value = "81";
         document.getElementById('sp').value = 2;
        document.getElementById('alk').value = 2;
        document.getElementById('cig').value = 3;
    }else{
            var at = preveri(hrime, ime, priimek);
            kaj.elements["visina"].value =  $("#dodajVitalnoTelesnaVisina").val();
          kaj.elements["teza"].value =  $("#dodajVitalnoTelesnaTeza").val();
       //  document.getElementById('sp').value = document.getElementById("dodajSpol").options[document.getElementById("dodajSpol").selectedIndex].value;
        //document.getElementById('alk').value = document.getElementById("dodajAlkohol").options[document.getElementById("dodajAlkohol").selectedIndex].value;
        //ocument.getElementById('cig').value = document.getElementById("dodajKajenje").options[document.getElementById("dodajKajenje").selectedIndex].value;
    }


}
   function izpolni() {
    document.getElementById("alert").innerHTML = "";
   var kaj = document.getElementById("data");  
   kaj.elements["fname"].value = "Leonardo";
   kaj.elements["lname"].value = "DiCaprio";
   kaj.elements["visina"].value = "183";
   kaj.elements["teza"].value = "75";
   kaj.elements["day"].value = "01";
   kaj.elements["month"].value = "11";
   kaj.elements["year"].value = "1974";
    document.getElementById('sp').value = 2;
    document.getElementById('alk').value = 2;
    document.getElementById('cig').value = 3;
}






function dodajMeritveVitalnihZnakov() {
	sessionId = getSessionId();
	var izbira = document.getElementById("dodajSpol");

	var ehrId = $("#dodajVitalnoEHR").val();
	var datumInUra = $("#dodajVitalnoDatumInUra").val();
	var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
	
	var spol = izbira.options[izbira.selectedIndex].value;
	
	var sistolicniKrvniTlak = 0;
	var diastolicniKrvniTlak = 0;
	var nasicenostKrviSKisikom = 0;
	var merilec = "haha";

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": spol,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}
function preberiMeritveVitalnihZnakov() {
	sessionId = getSessionId();

	var ehrId = $("#meritveVitalnihZnakovEHRid").val();
	var tip = 1;

	if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
          "podatkov za <b>'" + tip + "'</b> bolnika <b>'" + party.firstNames +
          " " + party.lastNames + "'</b>.</span><br/><br/>");
				if (tip == "telesna temperatura") {
					$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
						    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna temperatura</th></tr>";
						        for (var i in res) {
						            results += "<tr><td>" + res[i].time +
                          "</td><td class='text-right'>" + res[i].temperature +
                          " " + res[i].unit + "</td>";
						        }
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").append(results);
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				} else if (tip == "telesna teža") {
					$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/" + "weight",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
						    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna teža</th></tr>";
						        for (var i in res) {
						            results += "<tr><td>" + res[i].time +
                          "</td><td class='text-right'>" + res[i].weight + " " 	+
                          res[i].unit + "</td>";
						        }
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").append(results);
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				} else if (tip == "telesna temperatura AQL") {
					var AQL =
						"select " +
    						"t/data[at0002]/events[at0003]/time/value as cas, " +
    						"t/data[at0002]/events[at0003]/data[at0001]/items[at0004]/value/magnitude as temperatura_vrednost, " +
    						"t/data[at0002]/events[at0003]/data[at0001]/items[at0004]/value/units as temperatura_enota " +
						"from EHR e[e/ehr_id/value='" + ehrId + "'] " +
						"contains OBSERVATION t[openEHR-EHR-OBSERVATION.body_temperature.v1] " +
						"where t/data[at0002]/events[at0003]/data[at0001]/items[at0004]/value/magnitude<35 " +
						"order by t/data[at0002]/events[at0003]/time/value desc " +
						"limit 10";
					$.ajax({
					    url: baseUrl + "/query?" + $.param({"aql": AQL}),
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	var results = "<table class='table table-striped table-hover'>" +
                  "<tr><th>Datum in ura</th><th class='text-right'>" +
                  "Telesna temperatura</th></tr>";
					    	if (res) {
					    		var rows = res.resultSet;
						        for (var i in rows) {
						            results += "<tr><td>" + rows[i].cas +
                          "</td><td class='text-right'>" +
                          rows[i].temperatura_vrednost + " " 	+
                          rows[i].temperatura_enota + "</td>";
						        }
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").append(results);
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}

					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				}
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
}
$(document).ready(function() {

  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
  });

  /**
   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
   * ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
   */
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});

  /**
   * Napolni testne vrednosti (EHR ID, datum in ura, telesna višina,
   * telesna teža, telesna temperatura, sistolični in diastolični krvni tlak,
   * nasičenost krvi s kisikom in merilec) pri vnosu meritve vitalnih znakov
   * bolnika, ko uporabnik izbere vrednosti iz padajočega menuja (npr. Ata Smrk)
   */
	$('#preberiObstojeciVitalniZnak').change(function() {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("");
		var podatki = $(this).val().split("|");
		$("#dodajVitalnoEHR").val(podatki[0]);
		$("#dodajVitalnoDatumInUra").val(podatki[1]);
		$("#dodajVitalnoTelesnaVisina").val(podatki[2]);
		$("#dodajVitalnoTelesnaTeza").val(podatki[3]);
		$("#dodajVitalnoTelesnaTemperatura").val(podatki[4]);
		$("#dodajVitalnoKrvniTlakSistolicni").val(podatki[5]);
		$("#dodajVitalnoKrvniTlakDiastolicni").val(podatki[6]);
		$("#dodajVitalnoNasicenostKrviSKisikom").val(podatki[7]);
		$("#dodajVitalnoMerilec").val(podatki[8]);
	});

  /**
   * Napolni testni EHR ID pri pregledu meritev vitalnih znakov obstoječega
   * bolnika, ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Ata Smrk, Pujsa Pepa)
   */
	$('#preberiEhrIdZaVitalneZnake').change(function() {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("");
		$("#rezultatMeritveVitalnihZnakov").html("");
		$("#meritveVitalnihZnakovEHRid").val($(this).val());
	});

});
